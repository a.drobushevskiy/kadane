<?php

//$numbers = [4, 2, -5, 8, -3, -6, 3];

$maxSumSoFar      = 0;
$maxSumEndingHere = 0;

for ($i = 0; $i < 100; $i++) {
    $numbers[] = rand(-10, 10);
}

$count = count($numbers);

for ($i = 0; $i < $count; $i++) {
    $maxSumEndingHere = max($maxSumEndingHere + $numbers[$i], $numbers[$i]);

    if ($maxSumSoFar < $maxSumEndingHere) {
        $maxSumSoFar = $maxSumEndingHere;
    }
}

echo "Given array: [" . implode(', ', $numbers). "]\n";
echo "Maximum sum: " . $maxSumSoFar . "\n";
